package perez.frank.tl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import perez.frank.bl.entities.subasta.Subasta;
import perez.frank.bl.entities.usuario.Usuario;
import perez.frank.bl.logic.Gestor;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

    public class AdministradorController implements Initializable {
        ArrayList<Subasta> subastas = new ArrayList<>();
        Usuario usuarioLogerado = new Usuario();

        private Usuario usuarioSesion;
        private Gestor gestor = new Gestor();
        ObservableList<Object> datos = FXCollections.observableArrayList();

        @FXML
        private Button btnInicio;

        @FXML
        private Button btnItem;

        @FXML
        private Button btnSalir;

        @FXML
        private Button btnSubasta;

        @FXML
        private TableColumn colCinco;

        @FXML
        private TableColumn colCuatro;

        @FXML
        private TableColumn colDos;

        @FXML
        private TableColumn colNueve;

        @FXML
        private TableColumn colOcho;

        @FXML
        private TableColumn colSeis;

        @FXML
        private TableColumn colSiete;

        @FXML
        private TableColumn colTres;

        @FXML
        private TableColumn colUno;

        @FXML
        private Label lblTitulo;

        @FXML
        private TableView tblSubastas;

        @FXML
        private Label lblTitulo2;


        public Usuario getUsuarioSesion() {
            return usuarioSesion;
        }

        public void setUsuarioSesion(Usuario usuarioSesion) {
            this.usuarioSesion = usuarioSesion;
            setTitulo();
        }

        private void setTitulo(){
            lblTitulo.setText("Bienvenido " + usuarioSesion.getCedula() + " - " + usuarioSesion.getNombre());
        }

        public void mostrarInforSubasta() {
            //limpia la tabla antes de listar por si habian datos agregados anteriorimente
            tblSubastas.getItems().clear();
            //debe poner el nombre de las variables exactamente como viene en el objeto
            colUno.setCellValueFactory(new PropertyValueFactory("creador"));
            colDos.setCellValueFactory(new PropertyValueFactory("codigo"));
            colTres.setCellValueFactory(new PropertyValueFactory("fechaDeCreacion"));
            colCuatro.setCellValueFactory(new PropertyValueFactory("fechaDeInicio"));
            colCinco.setCellValueFactory(new PropertyValueFactory("fechaDeVencimiento"));
            colSeis.setCellValueFactory(new PropertyValueFactory("estado"));
            colSiete.setCellValueFactory(new PropertyValueFactory("precioDeInicio"));
            colOcho.setCellValueFactory(new PropertyValueFactory("listaOfertas"));
            colNueve.setCellValueFactory(new PropertyValueFactory("moderador"));

            for (int i = 0; i < subastas.size(); i++) {
                tblSubastas.getItems().add(subastas.get(i));
            }
        }



        @FXML

        public void salir(ActionEvent event) throws IOException{
            Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/Login.fxml"));
            Scene newScene = new Scene(parentScene);
            Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
            window.setScene(newScene);
            window.show();
        }

        @Override
        public void initialize(URL location, ResourceBundle resources) {
            try {
                usuarioLogerado = gestor.obtenerUsuarioLogeado();
                //subastas = gestor.listarSubastas();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mostrarInforSubasta();
            clickSubasta();
        }

        public void clickSubasta() {
            tblSubastas.setOnMousePressed(event -> {
                String subas = tblSubastas.getSelectionModel().getSelectedItem().toString().split(",")[0].replace("Subasta{codigo=","");
                System.out.println(subas);
                RegistroItem ri = new RegistroItem();
                ri.recibirDatos(subas);

                //Cambia a la pantalla de registro de item

                try {
                    Parent parentScene = null;
                    parentScene = FXMLLoader.load(getClass().getResource("../gui/RegistroItem.fxml"));
                    Scene newScene = new Scene(parentScene);
                    Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
                    window.setScene(newScene);
                    window.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            });
        }


        /*private void cargarListaUsuarios() throws Exception{

            tblPersonas.getItems().clear();

            //gestor.listarVendedores().forEach(persona -> datos.addAll());

            colUno.setCellValueFactory(new PropertyValueFactory<>("cedula"));
            colDos.setCellValueFactory(new PropertyValueFactory<>("nombre"));
            colTres.setCellValueFactory(new PropertyValueFactory<>("correo"));
            colCuatro.setCellValueFactory(new PropertyValueFactory<>("password"));

            tblPersonas.setItems(datos);
        }

        public void cargarUsuario(){
            Usuario usuario = (Usuario)tblPersonas.getSelectionModel().getSelectedItem();
            if(usuario !=null){
                txtCedula.setText(usuario.getCedula());
                txtNombre.setText(usuario.getNombre());
                //txtCorreo.setText(usuario.getCorreo());
                //txtPassword.setText(usuario.getPassword());
            }
        }*/

        public void registrarCategoria(ActionEvent event) throws Exception {
            Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/RegistroCategria.fxml"));
            Scene newScene = new Scene(parentScene);
            Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
            window.setScene(newScene);
            window.show();
        }

        public void modificarItems(ActionEvent event) throws IOException {
            Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/RegistroSubasta.fxml"));
            Scene newScene = new Scene(parentScene);
            Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
            window.setScene(newScene);
            window.show();
        }

        public void eliminarUsuario(ActionEvent actionEvent) {
        }

        @FXML
        void cargarSubasta(MouseEvent event) {
            Subasta subasta = (Subasta)tblSubastas.getSelectionModel().getSelectedItem();
            if(subasta !=null){
                //txtCreador.setText(subasta.getCreador().getCedula());
                //dateCreacion.setValue(subasta.getFechaDeCreacion());
                //txtPrecio.setLayoutX(subasta.getPrecioDeInicio());
            }
        }

        @FXML
        void registroSubasta(ActionEvent event) throws IOException {
            Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/RegistroSubasta.fxml"));
            Scene newScene = new Scene(parentScene);
            Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
            window.setScene(newScene);
            window.show();
        }

        /*public void modificarUsuario() throws Exception {
            gestor.modificarUsuario(txtCedula.getText(),txtNombre.getText(),txtCorreo.getText(),txtPassword.getText());
            cargarListaUsuarios();
        }

        public void eliminarUsuario() throws Exception {
            gestor.eliminarUsuario(txtCedula.getText());
            cargarListaUsuarios();
        }*/
}
