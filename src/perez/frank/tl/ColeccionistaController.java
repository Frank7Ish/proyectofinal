package perez.frank.tl;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import perez.frank.bl.logic.Gestor;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class ColeccionistaController implements Initializable {

    private PrintStream out = System.out;
    private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private Gestor gestor = new Gestor();

    @FXML
    private Button btnVendedor;

    @FXML
    private TextField txtNombre;

    @FXML
    private TextField txtApellido;

    @FXML
    private TextField txtApellido2;

    @FXML
    private TextField txtCedula;

    @FXML
    private TextField txtCorreo;

    @FXML
    private TextField txtEdad;

    @FXML
    private TextField txtEstado;

    @FXML
    private TextField txtImagen;

    @FXML
    private TextField txtIntereses;

    @FXML
    private DatePicker Date;

    @FXML
    private JFXButton btnImagen;

    @FXML
    private ImageView imagen;


    @FXML
    private PasswordField txtPassword;

    private double puntuacion = 0.0;

    private String urlImg = "";

    @FXML
    void registrarColeccionista(ActionEvent event) throws Exception {
        gestor.registrarColeccionista(txtCedula.getText(),txtNombre.getText(),txtApellido.getText(), txtApellido2.getText(), txtPassword.getText(), txtCorreo.getText(), urlImg, Date.getValue(), Integer.parseInt(txtEdad.getText()), puntuacion, txtIntereses.getText());
        Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/Login.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }

    @FXML
    void volver(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/Landing.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
       btnImagen.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Buscar Imagen");
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            // Agregar filtros para facilitar la busqueda
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Images", "."),
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png")
            );

            // Obtener la imagen seleccionada
            File imgFile = fileChooser.showOpenDialog(stage);

            // Mostar la imagen
            if (imgFile != null) {
                urlImg = imgFile.getName();
                System.out.println("imagen" + urlImg);
                Image image = new Image("file:" + imgFile.getAbsolutePath());
                imagen.setImage(image);
            }
        });
    }
}
