package perez.frank.tl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import perez.frank.bl.entities.usuario.Usuario;
import perez.frank.bl.logic.Gestor;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    private Gestor gestor = new Gestor();

    @FXML
    private TextField txtTipoUsuario;

    @FXML
    private TextField txtCedula;

    @FXML
    private PasswordField txtPassword;

    @FXML
    void volver(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/Landing.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }



    @FXML
    void validarDatos(ActionEvent event) {
        String cedula = txtCedula.getText();
        String contrasena = txtPassword.getText();
        Usuario usuario;
        boolean error = false;

        error = validarCampos(cedula, contrasena);

        if (error) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Por favor complete los campos");
            alert.setHeaderText(null);
            alert.showAndWait();
        } else {
            usuario = buscarUsuario(cedula, contrasena);
            if (usuario !=null) {
                cambiarEscena(usuario,event);
            }
            else{
                Alert alert = new Alert(Alert.AlertType.ERROR, "El usuario no se encuentra registrado");
                alert.setHeaderText(null);
                alert.showAndWait();
            }
        }
    }

    private boolean validarCampos(String cedula, String contrasena) {
        boolean error = false;

        if (cedula.equals("")) {
            error = true;
        }
        if (contrasena.equals("")) {
            error = true;
        }
        return error;
    }

    private Usuario buscarUsuario(String cedula, String password) {
        try {

            Usuario usuario = gestor.buscarUsuario(cedula, password);
            if(usuario != null) {
                return usuario;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void cambiarEscena(Usuario usuario, ActionEvent event){

        try {
            String tipoUsuario = "";
            if (usuario.getTiopUsuario().equals("Administrador")) {
                tipoUsuario = "AdminLogin.fxml";
            }
            if(usuario.getTiopUsuario().equals("Vendedor")){
                tipoUsuario = "PerfilVendedor.fxml";
            }
            if(usuario.getTiopUsuario().equals("Coleccionista")){
                tipoUsuario = "PerfilColeccionista.fxml";
            }

            Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/" + tipoUsuario));
            Scene newScene = new Scene(parentScene);
            Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
            window.setScene(newScene);
            window.show();

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Error al cambiar de ventana. Intente de nuevo: " + e.getMessage());
            alert.setHeaderText(null);
            alert.showAndWait();
        }
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    /*public void validarDatos(javafx.event.ActionEvent actionEvent) {
    }*/
}
