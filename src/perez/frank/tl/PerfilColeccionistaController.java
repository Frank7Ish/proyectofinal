package perez.frank.tl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import perez.frank.bl.entities.subasta.Subasta;
import perez.frank.bl.entities.usuario.Usuario;
import perez.frank.bl.logic.Gestor;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class PerfilColeccionistaController implements Initializable {

    @FXML
    private Button btnInicio;

    @FXML
    private Button btnItem;

    @FXML
    private Button btnSalir;

    @FXML
    private Button btnSubasta;

    @FXML
    private TableColumn colCinco;

    @FXML
    private TableColumn colCuatro;

    @FXML
    private TableColumn colDos;

    @FXML
    private TableColumn colNueve;

    @FXML
    private TableColumn colOcho;

    @FXML
    private TableColumn colSeis;

    @FXML
    private TableColumn colSiete;

    @FXML
    private TableColumn colTres;

    @FXML
    private TableColumn colUno;

    @FXML
    private Label lblTitulo;

    @FXML
    private TableView tblSubastas;

    @FXML
    private DatePicker dateCreacion;

    @FXML
    private TextField txtCreador;

    @FXML
    private TextField txtPrecio;

    ArrayList<Subasta> subastas = new ArrayList<>();
    Gestor gestor = new Gestor();
    Usuario usuarioLogerado = new Usuario();

    @FXML
    void registroSubasta(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/RegistroSubasta.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }

    @FXML
    void registroItem(ActionEvent event){

    }

    @FXML
    void salir(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/Landing.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }

    public void mostrarInforSubasta() {
        //limpia la tabla antes de listar por si habian datos agregados anteriorimente
        tblSubastas.getItems().clear();
        //debe poner el nombre de las variables exactamente como viene en el objeto
        colUno.setCellValueFactory(new PropertyValueFactory("creador"));
        colDos.setCellValueFactory(new PropertyValueFactory("codigo"));
        colTres.setCellValueFactory(new PropertyValueFactory("fechaDeCreacion"));
        colCuatro.setCellValueFactory(new PropertyValueFactory("fechaDeInicio"));
        colCinco.setCellValueFactory(new PropertyValueFactory("fechaDeVencimiento"));
        colSeis.setCellValueFactory(new PropertyValueFactory("estado"));
        colSiete.setCellValueFactory(new PropertyValueFactory("precioDeInicio"));
        colOcho.setCellValueFactory(new PropertyValueFactory("listaOfertas"));
        colNueve.setCellValueFactory(new PropertyValueFactory("moderador"));

        for (int i = 0; i < subastas.size(); i++) {
            tblSubastas.getItems().add(subastas.get(i));
        }
    }
    public void clickSubasta() {
        tblSubastas.setOnMousePressed(event -> {
            String subas = tblSubastas.getSelectionModel().getSelectedItem().toString().split(",")[0].replace("Subasta{codigo=","");
            System.out.println(subas);
            RegistroItem ri = new RegistroItem();
            ri.recibirDatos(subas);

            //Cambia a la pantalla de registro de item

            try {
                Parent parentScene = null;
                parentScene = FXMLLoader.load(getClass().getResource("../gui/RegistroItem.fxml"));
                Scene newScene = new Scene(parentScene);
                Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
                window.setScene(newScene);
                window.show();
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            usuarioLogerado = gestor.obtenerUsuarioLogeado();
            subastas = gestor.listarSubastas();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mostrarInforSubasta();
        clickSubasta();
    }
}
