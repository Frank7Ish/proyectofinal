package perez.frank.tl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import perez.frank.bl.entities.usuario.Usuario;
import perez.frank.bl.logic.Gestor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.util.ResourceBundle;

public class RegistroItem implements Initializable {
    private PrintStream out = System.out;
    private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private Gestor gestor = new Gestor();
    Usuario usuarioLogerado = new Usuario();

    @FXML
    private TextField txtNombre;

    @FXML
    private TextField txtEstado;

    @FXML
    private TextField txtDescripcion;

    @FXML
    private DatePicker dateCompra;

    @FXML
    private TextField txtAntiguedad;

    @FXML
    private TextField txtCategoria;

    @FXML
    private TextField txtPropietario;

    @FXML
    private TextField txtSubasta;

    private String urlImg = "";


    private String codSubasta;

    public void recibirDatos(String codigoSubasta){
        codSubasta = codigoSubasta;
    }

    @FXML
    void registrarItem(ActionEvent event) throws Exception {
        gestor.registrarItem(txtNombre.getText(),txtEstado.getText(),txtDescripcion.getText(), urlImg, dateCompra.getValue(), Integer.parseInt(txtAntiguedad.getText()), Integer.parseInt(txtCategoria.getText()), txtPropietario.getText(), Integer.parseInt(codSubasta));
        Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/PerfilVendedor.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /*try {
            usuarioLogerado = gestor.obtenerUsuarioLogeado();
            subastas = gestor.listarSubasta(usuarioLogerado.getCedula());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mostrarInforSubasta();
        clickSubasta();*/
    }

    @FXML
    void volver(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/PerfilVendedor.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }
}
