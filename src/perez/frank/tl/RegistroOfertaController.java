package perez.frank.tl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import perez.frank.bl.logic.Gestor;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class RegistroOfertaController implements Initializable {
    private Gestor gestor = new Gestor();

    @FXML
    private TextField txtOfertante;

    @FXML
    private TextField txtOferta;

    @FXML
    private TextField txtIdSubasta;

    @FXML
    void registrarOferta(ActionEvent event) throws Exception {
        gestor.registrarOfertaDeCompra(txtOfertante.getText(), Double.parseDouble(txtOferta.getText()), Integer.parseInt(txtIdSubasta.getText()));
        Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/Login.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }


    @FXML
    void volver(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/Landing.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
