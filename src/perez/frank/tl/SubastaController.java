package perez.frank.tl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import perez.frank.bl.logic.Gestor;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SubastaController implements Initializable {
    private Gestor gestor = new Gestor();

    @FXML
    private Button btnRSubasta;

    @FXML
    private TextField txtCreador;

    @FXML
    private DatePicker dateCreacion;

    @FXML
    private DatePicker dateInicio;

    @FXML
    private DatePicker dateFin;

    @FXML
    private TextField txtPrecio;

    @FXML
    private TextField txtModerador;

    @FXML
    void registrarSubasta(ActionEvent event) throws Exception {
        if (txtModerador.getText().equals("") || dateFin.getValue().equals("")) {
            //Poner un mensaje
        } else {
            gestor.registrarSubasta(txtCreador.getText(), dateCreacion.getValue(), dateInicio.getValue(), dateFin.getValue(), Double.parseDouble(txtPrecio.getText()));
            Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/PerfilVendedor.fxml"));
            Scene newScene = new Scene(parentScene);
            Stage window = (Stage) ((javafx.scene.Node) event.getSource()).getScene().getWindow();
            window.setScene(newScene);
            window.show();
        }

    }

    @FXML
    void volver(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/PerfilVendedorController.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage) ((javafx.scene.Node) event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
