package perez.frank.tl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import perez.frank.bl.logic.Gestor;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class VendedorController implements Initializable {

    private Gestor gestor = new Gestor();

    @FXML
    private Button btnVendedor;

    @FXML
    private TextField txtNombre;

    @FXML
    private TextField txtApellido;

    @FXML
    private TextField txtApellido2;

    @FXML
    private TextField txtCedula;

    @FXML
    private TextField txtCorreo;

    @FXML
    private TextArea txtDireccion;

    @FXML
    private PasswordField txtPassword;

   @FXML
    void registrarVendedor(ActionEvent event) throws Exception {
        gestor.registrarVendedor(txtCedula.getText(),txtNombre.getText(),txtApellido.getText(), txtApellido2.getText(), txtDireccion.getText(), txtCorreo.getText(), txtPassword.getText());
        Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/Login.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }


    @FXML
    void volver(ActionEvent event) throws IOException {
        Parent parentScene = FXMLLoader.load(getClass().getResource("../gui/Landing.fxml"));
        Scene newScene = new Scene(parentScene);
        Stage window = (Stage)((javafx.scene.Node)event.getSource()).getScene().getWindow();
        window.setScene(newScene);
        window.show();
    }
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
